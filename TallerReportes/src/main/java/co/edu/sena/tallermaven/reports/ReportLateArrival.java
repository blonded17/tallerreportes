/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.tallermaven.reports;


import co.edu.sena.tallermaven.utils.Constants;
import co.edu.sena.tallermaven.utils.MessageUtils;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Aprendiz
 */
public class ReportLateArrival extends ReportController {
    
    public ReportLateArrival() {
        super();
    }
public void executeLateArrivalReport(){
    try {
            
        String file = getClass().getResource(Constants.REPORT_LATEARRIVAL).getFile();
        System.out.println("Cargando reporte desde: "+file);
        if (file ==null) {
            MessageUtils.showErrorMessage("no se encuentra el archivo jasper");
            System.exit(2);
            
        }
        
        //reporte maestro
        JasperReport masterReport=null;
        try {
            masterReport =(JasperReport)JRLoader.loadObjectFromFile(file);
            
        } catch (JRException e) {
              MessageUtils.showErrorMessage("error al cargar el reporte "+e.getMessage());
              System.exit(3);
        }
        //llenar la conexion con base de datos 
        JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport,null,connect);
        //lanzar el view de jasper 
        //abre la ventana con el reporte general 
        JasperViewer jasperViewer = new JasperViewer(jasperPrint,false); //si esta en true cierra todo 
       jasperViewer.setTitle("reporte de aprendices");
       jasperViewer.setVisible(true);
        
        
    } catch (JRException e) {
        MessageUtils.showErrorMessage(e.getMessage());
    }
}
    



}
