package co.edu.sena.tallermaven.model;

import co.edu.sena.tallermaven.model.Apprentice;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-10-06T21:17:03", comments="EclipseLink-2.7.9.v20210604-rNA")
@StaticMetamodel(Course.class)
public class Course_ { 

    public static volatile SingularAttribute<Course, String> career;
    public static volatile CollectionAttribute<Course, Apprentice> apprenticeCollection;
    public static volatile SingularAttribute<Course, Integer> id;

}