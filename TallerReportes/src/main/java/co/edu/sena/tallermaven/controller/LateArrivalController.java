/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.tallermaven.controller;

import co.edu.sena.tallermaven.model.LateArrival;
import co.edu.sena.tallermaven.persistence.DAOFactory;
import co.edu.sena.tallermaven.persistence.EntityManagerHelper;
import java.util.List;

/**
 *
 * @author Blonded
 */
public class LateArrivalController {
    public void validate(LateArrival lateArrival) throws Exception {
        if (lateArrival == null) {
            throw new Exception("the late arrival is required");
        }
        if (lateArrival.getDateArrival() == null) {
            throw new Exception("the date is required");
        }
        if (lateArrival.getObservations().isEmpty()) {
            throw new Exception("the observation is required");
        }
        if (lateArrival.getId() == 0) {
            throw new Exception("the id is required");
        }
        if (lateArrival.getDocumentApprentice() == null) {
            throw new Exception("the apprentice document is required");
        }
    }
    public void insert(LateArrival lateArrival) throws Exception {
        validate(lateArrival);
        LateArrival oldLateArrival = DAOFactory.getLateArrivalDAO().
                find(lateArrival.getId());
        if (oldLateArrival != null) {
            throw new Exception("Already exist a apprentice with the same id ");
        }
        EntityManagerHelper.beginTransaction();
        DAOFactory.getLateArrivalDAO().insert(lateArrival);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }
    public void update(LateArrival lateArrival) throws Exception {
        validate(lateArrival);
        LateArrival oldLateArrival = DAOFactory.getLateArrivalDAO().
                find(lateArrival.getId());
        if (oldLateArrival == null) {
            throw new Exception("Doesn't exist the id to update");
        }
        
        oldLateArrival.setDateArrival(lateArrival.getDateArrival());
        oldLateArrival.setDocumentApprentice(lateArrival.getDocumentApprentice());
        oldLateArrival.setObservations(lateArrival.getObservations());
        
        EntityManagerHelper.beginTransaction();
        DAOFactory.getLateArrivalDAO().update(oldLateArrival);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }
    public void delete(LateArrival lateArrival) throws Exception {
        LateArrival oldLateArrival = DAOFactory.getLateArrivalDAO().
                find(lateArrival.getId());
        if (oldLateArrival == null) {
            throw new Exception("Doesn't exist the id to delete");
        }
        
        EntityManagerHelper.beginTransaction();
        DAOFactory.getLateArrivalDAO().delete(oldLateArrival);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }
    public LateArrival find(Integer id) throws Exception {
        if (id == 0) {
            throw new Exception("The id is required");
        }
        return DAOFactory.getLateArrivalDAO().find(id); 
    }    
    public List<LateArrival> findAll() throws Exception {
        return DAOFactory.getLateArrivalDAO().findAll();
    }
    
}
