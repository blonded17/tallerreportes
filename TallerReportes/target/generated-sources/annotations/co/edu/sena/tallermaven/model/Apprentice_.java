package co.edu.sena.tallermaven.model;

import co.edu.sena.tallermaven.model.Course;
import co.edu.sena.tallermaven.model.LateArrival;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-10-06T21:17:03", comments="EclipseLink-2.7.9.v20210604-rNA")
@StaticMetamodel(Apprentice.class)
public class Apprentice_ { 

    public static volatile SingularAttribute<Apprentice, Course> idCourse;
    public static volatile CollectionAttribute<Apprentice, LateArrival> lateArrivalCollection;
    public static volatile SingularAttribute<Apprentice, Long> document;
    public static volatile SingularAttribute<Apprentice, String> fullName;
    public static volatile SingularAttribute<Apprentice, String> email;

}