/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.tallermaven.reports;


import co.edu.sena.tallermaven.persistence.EntityManagerHelper;
import co.edu.sena.tallermaven.utils.MessageUtils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Map;


/**
 *
 * 
 */
public class ReportController {

    protected Connection connect = null;

    public ReportController() {
        try {
            Map<String, Object> properties = EntityManagerHelper.getEntityManager().getProperties();
            String driver = properties.get("javax.persistence.jdbc.driver").toString();
            String user = properties.get("javax.persistence.jdbc.user").toString();
            String password = properties.get("javax.persistence.jdbc.password").toString();
            String url = properties.get("javax.persistence.jdbc.url").toString();
            
            Class.forName(driver);
            connect=DriverManager.getConnection(url, user,password);
            Statement statement = connect.createStatement();
            System.out.println("Conexion establecida");
            
            
        } catch (Exception e) {
            MessageUtils.showErrorMessage(e.getMessage());
        }
    }
    
    public void close(){
        try {
            connect.close();
        } catch (Exception ex) {
             MessageUtils.showErrorMessage(ex.getMessage());
        }
    }
    

}
