package co.edu.sena.tallermaven.controller;

import co.edu.sena.tallermaven.model.Course;
import co.edu.sena.tallermaven.persistence.DAOFactory;
import co.edu.sena.tallermaven.persistence.EntityManagerHelper;
import java.util.List;

/**
 *
 * @author Blonded
 */
public class CourseController {
    public void validate(Course course) throws Exception {
        if (course == null) {
            throw new Exception("The course is required");
        }
        if (course.getCareer().isEmpty()) {
            throw new Exception("the career is required");
        }
        if (course.getId() == 0) {
            throw new Exception("the id is required");
        }
    }
    public void insert(Course course) throws Exception {
        validate(course);
        Course oldCourse = DAOFactory.getCourseDAO().
                find(course.getId());
        if (oldCourse == null) {
            throw new Exception("Already exist a apprentice with this id");
        }
        EntityManagerHelper.beginTransaction();
        DAOFactory.getCourseDAO().insert(course);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }
    public void update(Course course) throws Exception {
        validate(course);
        Course oldCourse = DAOFactory.getCourseDAO().
                find(course.getId());
        if (oldCourse == null) {
            throw new Exception("Doesn't exist the course to update");
        }
        oldCourse.setCareer(course.getCareer());
                
        EntityManagerHelper.beginTransaction();
        DAOFactory.getCourseDAO().update(oldCourse);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }
    public void delete(Course course) throws Exception {
        validate(course);
        Course oldCourse = DAOFactory.getCourseDAO().
                find(course.getId());
        if (oldCourse == null) {
            throw new Exception("I can't delete the course");
        }
        
        EntityManagerHelper.beginTransaction();
        DAOFactory.getCourseDAO().delete(oldCourse);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }
    public Course find(Integer id) throws Exception{
        if (id == 0) {
            throw new Exception("The id is required");
        }
        return DAOFactory.getCourseDAO().find(id);
    }
    public List<Course> findAll() throws Exception {
        return DAOFactory.getCourseDAO().findAll();
    }
}
