/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.tallermaven.controller;

import co.edu.sena.tallermaven.model.Apprentice;
import co.edu.sena.tallermaven.persistence.DAOFactory;
import co.edu.sena.tallermaven.persistence.EntityManagerHelper;
import java.util.List;

/**
 *
 * @author Blonded
 */
public class ApprenticeController {

    public void validate(Apprentice apprentice) throws Exception {

        if (apprentice == null) {
            throw new Exception("He apprentice is required");
        }
        if (apprentice.getDocument() == 0) {
            throw new Exception("The document is required");
        }
        if (apprentice.getFullName().isEmpty()) {
            throw new Exception("The full name is required");
        }
        if (apprentice.getIdCourse() == null) {
            throw new Exception("The course is required");
        }
        if (apprentice.getLateArrivalCollection().isEmpty()) {
            throw new Exception("The late arrival collection is required");
        }
    }

    public void insert(Apprentice apprentice) throws Exception {
        validate(apprentice);
        Apprentice oldApprentice = DAOFactory.getApprenticeDAO().
                find(apprentice.getDocument());
        if (oldApprentice != null) {
            throw new Exception("Alredy exist a apprentice with the same document");
        }
        EntityManagerHelper.beginTransaction();
        DAOFactory.getApprenticeDAO().insert(apprentice);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }

    public void update(Apprentice apprentice) throws Exception {
        validate(apprentice);
        Apprentice oldApprentice = DAOFactory.getApprenticeDAO().
                find(apprentice.getDocument());
        if (oldApprentice == null) {
            throw new Exception("Doesn't exist a apprentice with that document");
        }
        
        oldApprentice.setFullName(apprentice.getFullName());
        oldApprentice.setIdCourse(apprentice.getIdCourse());
        oldApprentice.setDocument(apprentice.getDocument());
        oldApprentice.setEmail(apprentice.getEmail());
        
        EntityManagerHelper.beginTransaction();
        DAOFactory.getApprenticeDAO().update(oldApprentice);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }
    public void delete(Apprentice apprentice) throws Exception {
        Apprentice oldApprentice = DAOFactory.getApprenticeDAO().
                find(apprentice.getDocument());
        if (oldApprentice == null) {
            throw new Exception("does not exist a apprentice with that document");
        }
        EntityManagerHelper.beginTransaction();
        DAOFactory.getApprenticeDAO().delete(oldApprentice);
        EntityManagerHelper.commit();
        EntityManagerHelper.closeEntityManager();
    }
    public Apprentice find(Long document) throws Exception {
        if (document == 0) {
            throw new Exception("The document is required");
        }
        return DAOFactory.getApprenticeDAO().find(document);
    }
    public List<Apprentice> findAll() throws Exception {
        return DAOFactory.getApprenticeDAO().findAll();
    }
}
