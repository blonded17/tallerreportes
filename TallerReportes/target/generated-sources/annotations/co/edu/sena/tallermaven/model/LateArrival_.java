package co.edu.sena.tallermaven.model;

import co.edu.sena.tallermaven.model.Apprentice;
import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2022-10-06T21:17:03", comments="EclipseLink-2.7.9.v20210604-rNA")
@StaticMetamodel(LateArrival.class)
public class LateArrival_ { 

    public static volatile SingularAttribute<LateArrival, Date> dateArrival;
    public static volatile SingularAttribute<LateArrival, Apprentice> documentApprentice;
    public static volatile SingularAttribute<LateArrival, String> observations;
    public static volatile SingularAttribute<LateArrival, Integer> id;

}