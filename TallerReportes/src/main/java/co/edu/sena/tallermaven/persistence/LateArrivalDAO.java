/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.tallermaven.persistence;

import co.edu.sena.tallermaven.model.LateArrival;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Blonded
 */
public class LateArrivalDAO implements ILateArrivalDAO{

    @Override
    public void insert(LateArrival lateArrival) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().persist(lateArrival);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(LateArrival lateArrival) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().merge(lateArrival);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void delete(LateArrival lateArrival) throws Exception {
        try {
            EntityManagerHelper.getEntityManager().remove(lateArrival);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public LateArrival find(Integer id) throws Exception {
        try {
            return EntityManagerHelper.getEntityManager().find(LateArrival.class, id);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<LateArrival> findAll() throws Exception {
        try {
            Query query = EntityManagerHelper.getEntityManager().
                    createNamedQuery("LateArrival.findAll");
            return query.getResultList();
        } catch (Exception e) {
            throw e;
        }
    } 
}
